# README

## Steps

### Start app 
`python manage.py startapp testapp`

#### Change the views to return the render result

### Connect to Mysql DB
#### Create the mysql DB 

https://www.geeksforgeeks.org/python-mysql-create-database/

`docker pull mysql:latest`

`docker run -d --name template-django -e MYSQL_ROOT_PASSWORD=template-django-passowrd -p 3306:3306 mysql:latest`

change settings.py DATABASES

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'your_database_name',
        'USER': 'root',
        'PASSWORD': 'your_mysql_root_password',
        'HOST': '127.0.0.1',  # Set to 'mysql-django' if both containers are on the same network
        'PORT': '3306',
    }
}
```
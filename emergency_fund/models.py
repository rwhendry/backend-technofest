import uuid

from django.db import models

# Create your models here.

class DanaDarurat(models.Model):
    class MaritalStatusChoices(models.IntegerChoices):
        SINGLE = 0
        MARRIED = 1
        MARRIED_WITH_CHILDREN = 2

    class PaymentType(models.IntegerChoices):
        MANUAL = 0
        AUTODEBIT = 1
        ALL = 2

    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    user_id = models.CharField(unique=True, max_length=32)
    bank_account_id = models.CharField(unique=True, max_length=32)
    expenses_per_month = models.IntegerField()
    emergency_fund_target = models.IntegerField()
    marital_status = models.IntegerField(choices=MaritalStatusChoices.choices)
    payment_type = models.IntegerField(choices=PaymentType.choices)

from django.urls import path

from . import views

urlpatterns = [
    path("test/", views.test, name="test"),
    path("register/", views.register, name='register'),
    path("login/", views.login, name='login'),
    path("get_account_info/", views.get_account_info, name='get_account_info'),
    path('emergency_fund/', views.emergency_fund, name='emergency_fund'),
    path('tabung/', views.tabung, name='tabung'),
    path('ambil/', views.ambil, name='ambil')
]

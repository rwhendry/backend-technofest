import json

import requests

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import DanaDarurat

API_URL = 'http://34.101.154.14:8175/hackathon/'


@csrf_exempt
def test(request):
    if request.method == 'POST':
        target_url = f'{API_URL}user/info/'
        print('masuk sini')

        response = requests.post(target_url, data={},
                                 headers={'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}'})
        print({'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}'})

        print(vars(response))

        return JsonResponse(response.json(), status=response.status_code)

    return HttpResponse("test")


@csrf_exempt
def register(request):
    print(request.body)
    if request.method == 'POST':
        target_url = f'{API_URL}user/auth/create/'

        data = json.loads(request.body)
        print(data['values'])

        response = requests.post(target_url, data=json.dumps(data['values']),
                                 headers={'Content-Type': 'application/json'})
        print(response.json())

        data = response.json()
        if data['success']:
            return JsonResponse(response.json(), status=response.status_code)
        else:
            return JsonResponse({'error': data['errMsg']}, status=response.status_code)

    return HttpResponse("GET")


@csrf_exempt
def login(request):
    print(request.body)
    if request.method == 'POST':
        target_url = f'{API_URL}user/auth/token/'

        data = json.loads(request.body)

        response = requests.post(target_url, data=json.dumps(data),
                                 headers={'Content-Type': 'application/json'})
        print(response.json())

        return JsonResponse(response.json(), status=response.status_code)

    return HttpResponse("GET")


@csrf_exempt
def get_account_info(request):
    if request.method == 'POST':
        target_url = f'{API_URL}user/info/'

        response = requests.post(target_url, data={},
                                 headers={'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}',
                                          'Content-Type': 'application/json'})
        print({'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}'})

        print(vars(response), 'masuk sini', response.status_code, response)

        return JsonResponse(response.json(), status=response.status_code)

    return HttpResponse("test")


@csrf_exempt
def emergency_fund(request):
    if request.method == 'POST':
        body_value = json.loads(request.body)['values']
        print(body_value)
        target_url = f'{API_URL}user/info/'
        response = requests.post(target_url, data={},
                                 headers={'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}',
                                          'Content-Type': 'application/json'})

        print({'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}'})

        data = response.json()
        uid = data['data']['uid']

        target_url = f'{API_URL}bankAccount/create/'
        response = requests.post(target_url, data=json.dumps({"balance": 0}),
                                 headers={'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}',
                                          'Content-Type': 'application/json'})

        data = response.json()
        account_id = data['data']['accountNo']

        DanaDarurat.objects.create(
            user_id=uid,
            bank_account_id=account_id,
            expenses_per_month=body_value['expensePerMonth'],
            emergency_fund_target=body_value['emergencyFundTarget'],
            marital_status=body_value['maritalStatus'],
            payment_type=body_value['paymentType']
        )

        return HttpResponse("Success")

    if request.method == 'GET':
        target_url = f'{API_URL}user/info/'
        response = requests.post(target_url, data={},
                                 headers={'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}',
                                          'Content-Type': 'application/json'})

        data = response.json()
        uid = data['data']['uid']

        dana_darurat = DanaDarurat.objects.get(user_id=uid)
        account_id = dana_darurat.bank_account_id

        target_url = f'{API_URL}bankAccount/info/'
        response = requests.post(target_url, data=json.dumps({'accountNo': account_id}),
                                 headers={'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}',
                                          'Content-Type': 'application/json'})

        data = response.json()
        balance = data['data']['balance']
        print(data['data']['balance'])

        return JsonResponse(json.dumps({
            'emergency_fund_target': dana_darurat.emergency_fund_target,
            'expenses_per_month': dana_darurat.expenses_per_month,
            'balance': balance
        }), status=response.status_code, safe=False)

    return HttpResponse("test")


@csrf_exempt
def tabung(request):
    if request.method == 'POST':
        body_value = json.loads(request.body)
        balance = body_value['balance']

        target_url = f'{API_URL}user/info/'
        response = requests.post(target_url, data={},
                                 headers={'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}',
                                          'Content-Type': 'application/json'})

        print({'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}'})

        data = response.json()
        uid = data['data']['uid']

        dana_darurat = DanaDarurat.objects.get(user_id=uid)
        account_id = dana_darurat.bank_account_id

        target_url = f'{API_URL}bankAccount/addBalance/'

        response = requests.post(target_url, data=json.dumps({'receiverAccountNo': account_id, 'amount': balance}),
                                 headers={'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}',
                                          'Content-Type': 'application/json'})
        data = response.json()

        print(data)
        dummy_account_no = 5859456195662065

        return JsonResponse({}, status=response.status_code)


@csrf_exempt
def ambil(request):
    if request.method == 'POST':
        target_url = f'{API_URL}user/info/'
        response = requests.post(target_url, data={},
                                 headers={'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}',
                                          'Content-Type': 'application/json'})

        print({'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}'})

        data = response.json()
        uid = data['data']['uid']

        dana_darurat = DanaDarurat.objects.get(user_id=uid)
        account_id = dana_darurat.bank_account_id

        target_url = f'{API_URL}bankAccount/info/'
        response = requests.post(target_url, data=json.dumps({'accountNo': account_id}),
                                 headers={'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}',
                                          'Content-Type': 'application/json'})

        data = response.json()
        balance = data['data']['balance']

        target_url = f'{API_URL}bankAccount/transaction/create/'

        dummy_account_no = 5859456195662065

        response = requests.post(target_url, data=json.dumps({'receiverAccountNo': dummy_account_no , 'senderAccountNo': account_id, 'amount': balance}),
                                 headers={'Authorization': f'{request.META["HTTP_AUTHORIZATION"]}',
                                          'Content-Type': 'application/json'})
        data = response.json()

        print(data)

        return JsonResponse({}, status=response.status_code)
